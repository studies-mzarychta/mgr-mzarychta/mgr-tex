\section{Motivation}

\noindent Due to the~growing expectations regarding the~network size and bandwidth, the~technological progress and the~suggestions from the~\as{wr} community, the~next generation of the~\al{wrs} has become an~inevitable need. Besides replacing existing \as{cpu}/\as{fpga} architecture with \af{soc} technology, brand-new mainboard design and mechanical modifications concerning power supply, fans or physical interfaces~\cite{sevelsols-report}, one of the~project's primary goals is to provide support for 10~Gigabit~Ethernet and industry-level reliability features~\cite{wrs-wiki-v4}. The figure \ref{fig:switch-inside} presents the top view of \af{pcb} of the~WR~Switch~v3.3 device produced by Seven~Solutions company. The~4th version of the~switch hardware is currently under design, and new versions of gateware and software are on the~roadmap~\cite{wrs-wiki-v4}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.6]{pic/wrs3_03-v3.3-top-wiki.jpg}
	\caption{The top view of PCBs of the WR Switch v3.3 device produced by Seven Solutions company. \textit{Source:}~\cite{wrs-wiki-v3}.}
	\label{fig:switch-inside}
\end{figure}

The~\as{fpga} device chosen for the~project is the~Xilinx Zynq UltraScale+ MPSoC~\cite{sevelsols-report}. Using \as{soc} technology and much more programmable logic resources than in the~current release of the~gateware, implemented on Xilinx~Virtex-6 XC6VLX240T, enables expanding the~switch functionalities and adding enhancements that may boost its performance. Moreover, increasing port speed to 10~Gb/s implies that the~scope of the~necessary changes is more comprehensive than only porting the~existing solution to the~new architecture.

One of the~parts of the~existing gateware requiring substantial improvements is the~module responsible for making the~forwarding decision, which is the~bottleneck of the~switch processing path \cite{lipinski}. As a~consequence of data rate change, the~maximum possible number of requests per second will increase by at least an~order of magnitude (assuming the~same number of switch ports). Besides, as outlined in section~\ref{wrs-forwarding-engine}, the~current Full Lookup Engine module has drawbacks and limitations that should be addressed. Chosen lookup algorithm and lack of appropriate pipelining result in an~indeterministic response generation. Supporting a~forwarding decision with input from the~Fast Lookup Engine module is a~workaround that ensures switch determinism, but the Fast Lookup Engine configuration is limited to the~traffic defined as critical.

\section{Goals and Achievements}

\noindent The~goal of~the thesis was to provide an~efficient forwarding engine for a~new release of the~\as{wrs} gateware, taking advantage of the~\as{soc} technology and available resources. Concurrently, it was 
desired to achieve as much backward compatibility as possible, especially regarding module interfaces. Such an attitude will simplify the~integration of the~developed module with the~rest of the~current \as{wrs} firmware. The~solution was supposed to be future-proof and modular. The~project was developed as an~open-source gateware, licensed under \al{ohl} as the~rest of the~\al{wr} project components.

The~level of ambition was to analyse all known solutions in the~area of implementing forwarding engines in the~programmable logic, including the~cutting-edge architectures that allow combining high performance and low resources usage. The~results of the~research, presented in chapter \textit{\nameref{state-of-art}}, are valuable in the~context of implementing any exact matching search engines.

Besides, the~scope of the~thesis comprised becoming acquainted with the~overall \as{wrs} architecture to implement the~forwarding engine module that best suits the~new version of the~\as{wrs} gateware needs, as outlined in chapters \textit{\nameref{fib-implementation}} and \textit{\nameref{engine-implementation}}.

\section{Technical Assumptions}

\noindent The~list below presents the~next generation \al{wrs} gateware initial technical assumptions imposed by \as{cern}. Those~assumptions constitute the~guidelines and the~limitations for the~thesis and have a~direct impact on the~concept and the~implementation details of the~forwarding engine.
\begin{itemize}
	\item Implementation on the~Xilinx Zynq Ultrascale+ XCZU17EG-1FFVC1760E device.
	\item Switch hardware with up to 24 10 Gigabit Ethernet ports.
	\item Assuming full bandwidth support on each port.
	\item Parametrized number of switch ports in the~\as{hdl} project.
	\item Full compatibility with the~\as{ieee}~802.1Q standard.
	\item Deterministic switch latency for all types of traffic.
	\item Filtering engine module should work properly with at least 160~MHz frequency\footnote{The~expected frequency results from the~fact that the~GTH transceivers operate at 156.25~MHz.}.
	\item Using on-chip \af{bram} or \af{uram} memory\footnote{A \al{bram} is a~random access memory implemented in programmable logic, providing an access in one clock cycle. It is seen by the~operating system in its memory map, and physical access to it is provided by the~\as{axi}4 interface. In some cases, the~\as{bram} may be replaced by \al{uram}. \as{uram} can be treated as a~two-port \as{bram} memory with greater granularity (8 times), with a~constant port width (72 bits) and with a~single clock for both memory ports.} for storing \\a~Filtering Database.
	\item Two-thirds of the~programmable logic's memory resources is reserved for switch frames' buffers, and the~remaining one third may be utilized for a~Filtering Database.
	\item Up to 10K entries in a~Filtering Database.
	\item Communication between programmable logic and processing system through\\ \af{axi}\footnote{The AXI is the~part of the ARM Advanced Microcontroller Bus Architecture specifications. It is a synchronous, high-performance, multi-master, multi-slave communication interface. AXI4, AXI4-Lite and AXI4-Stream interface types are adopted by Xilinx as main communication buses in their devices.}. 
	\item Applying a~converter between \as{axi} and Wishbone interfaces in the~case of testing\\ the~developed forwarding engine with the~current release of the~\as{wrs} gateware.
	\item Filtering Database atomic updates in the~meantime.
	\item Percent of available \as{fpga} resources utilized for the~forwarding engine is supposed to be comparable to the~corresponding ratio in a~current \as{wrs} gateware release. 
\end{itemize}
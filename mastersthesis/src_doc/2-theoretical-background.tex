\noindent The~subject presented in this thesis requires the~basic understanding of \af{lan} switching technology concepts, the~\as{ieee}~802.1Q standard and general switch architecture. As this field of knowledge is broad, the~chapter focuses only on the~aspects closely related to the~thesis topic and aims at giving the~appropriate theoretical background for the~provided forwarding engine implementation.  

\section{LAN Switch Technology}

\noindent In crude terms, a~switch is used to interconnect \al{lan}s, defined as either wired or wireless data~communication networks connecting various terminals or computers within a~limited geographical area. Although \as{lan} technology comprises many different solutions, only some are formalized and approved by official standards organizations. The~wired computer networking \as{lan} technologies that have achieved widespread use include Ethernet, Token Ring and \af{fddi}, of which Ethernet is the~most common one \cite{all-switch}.

\subsection{Switching and the~OSI Model}
\label{ss:osi}

\noindent Typically, \textit{switch} refers to the~device that operates only at two first layers of the~\af{osi} model -- the~physical layer and the~data~link layer -- and uses \as{mac} addresses of the~host's \afp{nic} to forward data. The~\as{osi} model diagram is depicted in figure \ref{fig:osi}. 

\begin{figure}[h]
	\centering
	\includegraphics[scale=1]{pic/osi.pdf}
	\caption{The~\as{osi} model diagram. \textit{Source:} based on \cite{all-switch}.}
	\label{fig:osi}
\end{figure}

This chapter concentrates on such ordinary network switches and their bridging functionality\footnote{Names \textit{switch} and \textit{bridge} may be used interchangeably. Early \as{lan} bridges had two ports and performed frame forwarding using software only. Since technology has evolved to the~point where \as{lan} bridges process frames using hardware and are capable of forwarding frames at wire-speed on many ports, they have become called switches.} even though there are also \textit{multi-layer switches} with functionalities of higher \as{osi} layers.\footnote{The~example is a~\af{l3} switch, which also provides the~routing functionality and sends packets to a~specific next-hop \as{ipprot} address based on the~packet destination \as{ipprot} address.} The example of the Ethernet switch device, WR Switch v3.4 produced by Seven Solutions company, is presented in the figure \ref{fig:switch}.

The~physical layer is responsible for the~actual physical connection between devices and is concerned with a~transmission medium and mechanical, electrical and timing interfaces. Its functions comprise bit synchronization, bit rate control and defining a~transmission mode.

The~data~link layer performs the~reliable node to node delivery of data. It consists of two sublayers: \af{llc} and \af{mac}. The~\as{llc} is the~upper sublayer, independent of underlying \as{lan} technology, which ensures control and multiplexing for the~higher-layer clients. The~\as{mac} sublayer ensures control and multiplexing for the~transmission medium. Altogether, the~data~link layer provides mechanisms for:
\begin{itemize}
	% in case of Ethernet - adding preamble, start frame delimiter, interpacket gap
	% encapsulation of the~network layer data~packets into frames
	\item framing,
	% in case of Ethernet - adding MAC addresses to frames
	\item physical addressing,
	% usually by means of generating and checking frame check sequences
	\item error control,
	% coordinating the~amount of data~that can be sent before receiving acknowledgement
	\item flow control,
	% determining which device has control over the~channel at a~given time
	\item channel access control.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{pic/wrs3_W01-wrs3.4-iso.jpg}
	\caption{The example of the Ethernet switch device. \textit{Source:}~\cite{wrs-wiki-v3}.}
	\label{fig:switch}
\end{figure}

\subsection{Principles of Transparent Bridge Operation}
\label{transparent-bridges}

\noindent The~transparent bridge is the~type of bridge used on all types of \as{lan}s, including Ethernet networks. Its name comes from its operation principles -- no changes in hosts connected by a~switch are required, and it does not modify forwarded frames. Thus hosts are unaware of the~presence of a~bridge.\footnote{The~opposite of a~transparent bridge is a~source routing bridge, used exclusively on Token Rings and some \as{fddi} networks. Source routing bridges do not maintain any routing information, which is stored in the~frames instead. Besides, the~route over which a~frame is supposed to be sent must be known to every station in the~\as{lan} network. On the~other hand, such a~system ensures an~optimal path for the~frames and decreases the~processing delay.}
The~formal description of transparent bridge operation was initially defined in the~\as{ieee}~802.1D standard (\textit{Media~Access Control (MAC) Bridges}). However, by 2014 it has been incorporated into the~\as{ieee} 802.1Q standard (\textit{Bridges and Bridged Networks}). The~principal elements of bridge operation include \cite{ieee8021q}: 
\begin{itemize}
	\item relay and filtering of frames,
	\item maintenance of the~forwarding information base through address learning and ageing,
	\item bridge management.
\end{itemize}

The~forwarding information base, also known as the~forwarding table or \as{mac} table, maps \as{mac} addresses to device ports, thus supporting queries to determine whether received frames with a~given destination \as{mac} address are supposed to be forwarded through a~given transmission port. There are two types of forwarding table entries: static, which are explicitly configured as the~part of bridge management and cannot be automatically modified or removed, and dynamic, created during regular operation of a~bridge. Initially, a~table contains solely static entries. 

When a~frame is received on any port, its destination \as{mac} address is searched in a~\as{mac} table. The~found entry determines the~output port for a~frame. The~bridge also performs a~filtering process -- if the~port number in a~table entry is equal to the~port of arrival and the~destination address belongs thereby to the~same segment as the~source address, the~frame is discarded. In the~case of receiving a~frame with an~unknown or multicast destination \as{mac} address, a~frame is sent to all ports except the~one on which it arrived. Such an~operation is known as flooding and generates excess traffic.

If the~multicast traffic is intensive, the~inefficiency of flooding may become problematic. Therefore, the~multicast pruning mechanisms are employed to confine multicast traffic to areas of the~network where it is required. The~\af{mmrp}\footnote{The~\al{mmrp} was introduced with the~\as{ieee}~802.1ak amendment in 2007 to replace the~\af{gmrp}. Nowadays, it is part of the~\as{ieee}~802.1Q standard.} is used to register group MAC addresses on multiple switches, thus managing explicitly the~Filtering Database entries mapping multicast addresses to bridge ports. The~point is to perform \as{mac} table lookup even in the~case of multicast frames and send such frames to only those ports that are necessary for the~delivery of specific multicast traffic to all end stations, which are supposed to receive it, instead of flooding.

A bridge also performs a~table lookup for an~entry corresponding to a~source \as{mac} address, indicating a~host that has sent a~frame, with the~aim of a~learning process. If the~address is found, the~associated port mapping is potentially updated, which allows the~bridge to map correctly end stations that have changed a~\as{lan} segment. Otherwise, a~new dynamic entry that specifies a~reception port for the~frame's source address is created to build the~table for future forwarding.

Maintaining a~lookup table also involves a~background mechanism of ageing out the~entries that have been not accessed as corresponding to the~source \as{mac} address of a~received frame for a~programmable amount of time. Invalidating inactive addresses takes account of changes in the~active topology of the~network. It ensures that end stations moved to a~different
part of the~network will not be permanently prevented from receiving frames~\cite{ieee8021q}. Besides, as detailed in chapter \ref{state-of-art}, for many lookup algorithms, the~more populated a~table, the~longer it can be searched. Hence keeping stale entries in a~lookup table may have an~adverse impact on a~bridge performance.

Another important issue that should be addressed is the~risk of a~switching loop, which occurs when more than one link exists between two endpoints in a~network. The~occurrence of a~loop can cause confusing \as{mac} tables' entries and forwarding flooded traffic indefinitely\footnote{There is no concept of \textit{time to live} at the~data~link layer, which would limit the~lifetime of data~in a~network. Therefore, in the~event of a~broadcast storm, frames are forwarded until they are dropped, for example due to resource exhaustion.}, affecting an~entire network's performance. Since the~physical network topology with switching loops is often needed for redundancy reasons, the~loop resolution can be secured at a~logical topology level with the~use of dedicated technologies\footnote{For instance, \af{rstp}, \af{mstp}, or \af{spb} protocols, specified in \mbox{the~\as{ieee}~802.1Q-2014} standard.}. 

% TODO port mirroring?

\subsection{Virtual LANs}
\label{ss:vlan}

\noindent \Afp{vlan}, introduced in the~\as{ieee}~802.1Q standard, allow separating logical connectivity from physical connectivity. The~networks can be partitioned into separate \as{vlan}s on the~basis of functional requirements, network protocols or high-level applications -- there are no restrictions on the~nature of grouping. The~\as{vlan}s can be created across multiple switches beyond the~location limits. The~end stations belonging to a~\as{vlan} can behave as if they are connected to a~single physical \as{lan} even when, in fact, they are not. 

Although supporting \as{vlan}s implies implementation and processing overhead in comparison to devices connected through a~single bridge, the~\as{vlan} technology brings many benefits. Constraining the~traffic to logically separated members of the~specific \as{vlan} enhances the~\as{lan} security and preserves the~bandwidth in the~case of multicast and unknown unicast traffic. Besides, thanks to independence from a~physical connection point, end stations' mobility is increased.

To benefit from the~\as{vlan} technology, network switches must be \textit{\as{vlan}-aware}, which implies an~additional set of features and capabilities in comparison to \textit{\as{vlan}-unaware} devices. Essentially, a~\as{vlan} configuration along with a~\as{mac} table contents must be taken into account when making a~forwarding decision. 

From the~switches perspective, a~\as{vlan} membership is determined on a~frame basis, and each frame belongs to a~single \as{vlan}. If a~frame contains a~\af{vid}, as depicted in figure \ref{fig:vlan-frame}, the~membership is indicated explicitly, which is known as an~\textit{explicit tagging}. Otherwise, a~bridge parses a~frame and associates it with a~specific \as{vlan} in compliance with the~configured rules, which may concern \as{mac} source address, protocol type and so forth. The~default bridge behaviour, defined in the~\as{ieee}~802.1Q standard, is the~port-based \as{vlan} mapping. This most straightforward \as{vlan} association rule requires no frame parsing -- untagged or priority-tagged\footnote{The~802.1Q \as{vlan} tag also carries a~frame's priority information to provide \af{qos} at the~media~access control level, as specified in the~\as{ieee}~802.1p standard. The~priority information is not related to the~\as{vlan} concept. The~value \texttt{0x000} of a~\as{vlan} identifier indicates the~priority tag, which informs solely about a~priority and does not indicates a~\as{vlan} association.} frame is associated with the~\as{vlan} identifier configured for the~switch port on which the~frame arrives. 

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{pic/vlan.pdf}
	\caption{An Ethernet frame with the~802.1Q tag.}
	\label{fig:vlan-frame}
\end{figure}

Similarly to the~information about \as{mac} addresses mapping, \as{vlan} association rules are stored in the~Filtering Database. They may be configured manually by a~bridge administrator or learnt dynamically through a~mechanism such as the~\af{mvrp}\footnote{The~\al{mvrp} was introduced with the~\as{ieee}~802.1ak amendment in 2007 to replace the~\af{gvrp}. Nowadays, it is part of the~\as{ieee}~802.1Q standard.}, which aims at sharing \as{vlan} information automatically between devices in the~network. According to the~\as{ieee}~802.1Q nomenclature, the~information related to \as{mac} addresses is filtering information, and the~information related to \as{vlan}s is registration information. The~registration information in the~Filtering Database defines the~\as{vlan} membership -- the~set of ports through which members of the~specific \as{vlan} can be reached.

In a~\as{vlan}-aware bridge, the~destination \as{mac} address is combined with the~\as{vlan} identifier to determine the~relevant output ports and make a~forwarding decision. The~rule concerns unicast and multicast addresses. Thus multicast forwarding can be controlled on a~per-\as{vlan} basis. A \as{mac} address stored in a~Filtering Database is associated with a~\af{fid}, which can identify either a~single \al{vid} or a~set of \al{vid}s. Assigning multiple \as{vid}s to an~\as{fid} allows \textit{shared \as{vlan} learning} -- \as{mac} address learnt from Ethernet frame with an~associated \al{vid} is used for forwarding frames with other \as{vid}s which are mapped to the~same \as{fid}. In the~opposite configuration strategy, called \textit{independent \as{vlan} learning}, the~\as{mac} address to port mapping information is learnt individually for each \al{vid}. It enables, for instance, assigning frames transmitted by a~single station to independent active topologies~\cite{ieee8021q}.

Additionally, a~configured \as{vlan} member sets can be used to apply ingress and egress filters for the~frames on the~basis of their \as{vid}s. The~ingress filter discards any received frame associated with a~\as{vlan} for which the~port of arrival is not in the~member set. Invalid frames are not submitted for further processing in a~forwarding engine. The~egress filter provides the~final validation check not to send inappropriate frames on an~output port. It protects from the~inconsistent \as{vlan} configuration, which may result from shared \as{vlan} learning. Besides, the~appropriate configuration of \as{vlan} member sets together with ingress and egress filters prevents loops in a~network topology.

\section{LAN Switch Architecture}

\noindent The~detailed \as{lan} switch architecture is undoubtedly device-specific and depends, for instance, on the~scope of implemented functionalities. Nevertheless, main concepts and functional models are common and will be outlined in this section, with the~assumption of supporting the~full range of bridge capabilities discussed in the~previous sections.

This brief overview of a~bridge design covers the~parts of a~design that are most often implemented in hardware. It omits all facets of the~switch operation that do not have severe timing constraints and are implemented rather in software, including network management, support for loop resolution protocols, traffic monitoring, internal diagnostics or maintenance. The general schema of the~\as{ieee}~802.1Q switch operation flow, described in following sections, is depicted in figure \ref{fig:switch-architecture}. 

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.66]{pic/switch.pdf}
	\caption{The \as{ieee}~802.1Q switch operation flow schema.}
	\label{fig:switch-architecture}
\end{figure}

\subsection{Ingress Flow}
\label{ss:ingress-flow}

\noindent The~switch ingress flow comprises processing data~from receiving them from the~attached \as{lan}s to the~point where a~forwarding decision is made. In the~first place, the~bridge port module provides the~physical layer service and converts electrical or optical signals from a~transmission medium to an~appropriate bit representation, dependently on the~particular \as{lan} technology. If~there are multiple physical interfaces and a~single logical interface for further frame processing, the~links must be aggregated, possibly with some kind of frames scheduling mechanism.

In the~next step of the~sequential processing path, frames are parsed and classified. The~classification rules are contingent on configured criteria and administrative policies, but most often, the~classification engine implements~\cite{all-switch}:
\begin{itemize}
	\item forwarding frames intended for local sinking to the~operating system\footnote{The~local sinking concerns the~traffic supposed to be processed in software, such as protocols for loop resolution, bridge management or automatic bridge configuration. Types of frames are recognized based on their specific multicast destination \as{mac} addresses or the~port number in the~application layer protocol.},
	\item applying the~\as{vlan} ingress rules and determining the~\as{vlan} membership of frames, as~discussed in the~\ref{ss:vlan} section,
	\item the~assessment of frames' priority.
\end{itemize}

The~classification engine implementation depends mainly on the~number of ports aggregated prior to classification, the~data rate of the~attached ports and the~level of classification rules' complexity. The~result of the~described process may be relayed in the~form of a~classification vector with the~essential frame information to avoid parsing it again at later stages.

The~incoming frames are filtered according to the~\as{vlan} ingress filter and, optionally, the~Acceptable Frame Types filter. The~latter is usually related to the~role of a~specific switch in a~network -- whether it is a~core switch or an~edge switch. It may be configured to admit: all frames, only tagged frames or only untagged and priority-tagged frames~\mbox{\cite[p. 73]{ieee8021q}}.

The~frame which passed the~preceding stages successfully is proceeded to the~detailed filtering process.
The~frame itself is buffered and its key attributes are used to make a~forwarding decision and determine the~output ports for transferring the~frame. Retrieving information related to the~\as{vlan} membership solely is uncomplicated, as the~value of the~12-bit \al{vid} is in the~range 1-4094\footnote{The~\texttt{0x000} and \texttt{0xFFF} values are reserved.}, and a~simple key-value mapping may be used. The~combination of destination \as{mac} address and the~\al{fid} can take many more values (at least $2^{48+1}$). Therefore a~direct search based on the~value is not an~option due to memory resource constraints, and efficient algorithms of storing and searching Filtering Database are a~necessity. The~subject of a~forwarding engine and the~Filtering Database architectures is discussed in more detail in chapter \ref{state-of-art}.

\subsection{Switch Fabrics}
\label{switch-fabrics}

\noindent In general, received frames have to be stored in memory in two states:
\begin{itemize}
	\item when the~processing of a~frame by classification or forwarding engine is not completed yet,
	\item when a~frame is already inserted into the~appropriate output queue(s) and is awaiting transmission. 
\end{itemize}  

The~function of buffering frames and transferring them among all of the~incoming and destination ports of a~switch is performed by \textit{switch fabrics}. The~design of internal switch fabrics is, along with the~classification and forwarding engine architectures, crucial in terms of switch performance. The main facets that should be considered comprise:
\begin{itemize}
	\item type of memory for storing frames, its bandwidth and latency,
	\item arbitration algorithms,
	\item the ability to send frames to multiple output ports,
	\item independent buffers for different classes of service,
	\item buffer organization (allocating contiguous buffers for maximum-length frames or storing it as a~series of segments
scattered throughout memory),
	\item methods of resolving head-of-line blocking (the problem of incapability of sending frames intended for unoccupied output ports because of waiting for transmission of the~frames intended for congested ones).
\end{itemize}

Regardless of the~implementation of the~fabrics, the~latency introduced by a~switch is lowest when frames are forwarded as soon as possible. The~aspect that greatly influences the~latency is 
a \textit{cut-through} switch operation mode. When it is enabled, a~frame may be transmitted even if it has not been fully received at the~input and its \af{fcs} has not been verified yet. On the~contrary, if only \textit{store-and-forward} mode is possible, a~whole frame is buffered before sending it on an~output port. However, it should be noticed that any benefit occurs only when the~output port is available\footnote{In the~case of unknown or multicast destination addresses the cut-through mode can be used only when all output ports are simultaneously available.}. Thus, the cut-through mode is an~addition to typical switch operation, not an~alternative.

\subsection{Egress Flow}
\label{ss:egress-flow}

\noindent The~egress flow in a~switch concerns the~path between reading frames from switch fabrics and potentially transmitting them to output ports. Despite the~forwarding decision is already made, the~egress filtering and applying the~egress rules are performed before sending frames onto the~physical links.

Because some devices, especially end stations, may be \as{vlan}-unaware, the~egress rules determine whether the~frame is supposed to be sent tagged or untagged. If necessary, a~switch removes the~\as{vlan} tag from a~frame and recalculates a~\as{fcs}. The~\as{vlan} tag may also be inserted at this point, usually when a~frame is untagged and an~egress port is used to connect to another \as{vlan}-aware switch. Furthermore, as explained in section~\ref{ss:vlan}, the~egress filtering ensures that the~output port's \as{vlan} membership is consistent with~the \al{vid} associated with the~frame. 

At this stage of processing, frames are passed to output queues associated with the~given output port. Except for the~traffic processed in the~switch core, the~switch processor may locally source frames and insert them into output queues. The~egress flow is a~part of the~switch design, in which the~priority of the~frame, as determined in the~classification engine, is taken into account, and the~prioritization of different classes of traffic is performed. 

The~egress port provides the~\as{mac} and the~physical layers' services and, ultimately, converts the~data stream into signals appropriate for the~transmission medium. Additionally, it may implement the~frame preemption technology specified in the~\as{ieee}~802.1Qbu standard, which provides mechanisms for interrupting the~transmission of non-critical frames. It is one of the~\af{tsn} standards, defining \mbox{the~time-sensitive} transmission of data over deterministic Ethernet networks. 




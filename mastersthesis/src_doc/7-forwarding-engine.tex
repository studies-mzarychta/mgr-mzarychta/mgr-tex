\section{Forwarding Decision Algorithm}
\label{forwarding-decision-algorithm}

\noindent The implemented forwarding decision algorithm is consistent with the~\as{ieee}~802.1Q~standard and its theoretical background has been presented in section~\ref{ss:ingress-flow}. The details of the~algorithm impact overall forwarding engine architecture. The forwarding decision flowchart is depicted in figure \ref{fig:forwarding-decision} and its description is presented below.\\

\noindent \textbf{Initial configuration verification}
\begin{enumerate}
	\item The~data set of the~forwarding engine request is specified in \ref{t:request}.
	\item Forwarding engine configuration, specified in \ref{t:global-params} and \ref{t:port-params}, is verified. Request is processed if global enable for forwarding engine is set and if request port is configured in either \textit{pass~all} or \textit{pass~local~link} mode. Otherwise the~frame associated with the~request is supposed to be dropped.
\end{enumerate}

\noindent \textbf{VLAN configuration verification}
\begin{enumerate}[resume]
	\item The \al{vtab} entry for \al{vid} from the~request is read.
	\item If the~\al{vid} from the~request is regarded as illegal according to the~configuration kept in the~\al{vtab}, the~frame associated with the~request is dropped.
\end{enumerate}

\noindent \textbf{Configuration for source MAC address verification}
\begin{enumerate}[resume]
	\item The~\al{htab} entry for source \as{mac} address from the~request and the~\al{fid} read previously from the~\al{vtab} is searched.
	\item If the~entry is not found in the~\al{htab}, the~source \as{mac} address from the~request and its associated source port~ID are written to the~\al{htab} within a~\textit{learning process}, which is described in section~\ref{transparent-bridges}. For this purpose, the~data set specified in \ref{t:learning-fifo} is written to the~appropriate buffer, provided that learning process for the~specific port~ID is enabled and there is a place in the~buffer. The buffer is read by the~\as{cpu}, which manages the~contents of the~\al{htab}. 
	\item If the~entry is found and has the~\textit{drop when source} flag set, the~frame associated with the~request is dropped.
	\item The \textit{port source mask} from the~\al{htab} entry is compared with the~source port~ID. In case of an inconsistency and the~\textit{drop unmatched} flag from the~\al{htab} entry set, the~frame associated with the~request is dropped.
\end{enumerate}

\noindent \textbf{Configuration for destination MAC address verification}
\begin{enumerate}[resume]
	\item The~\al{htab} entry for destination \as{mac} address from the~request and the~\as{fid} read previously from the~\al{vtab} is searched.
	\item If the~entry is not found in the~\al{htab}, the~frame associated with the~request is supposed to be broadcasted, provided that broadcasting unrecognized frames is enabled for the~source port ID -- otherwise the~frame associated with the~request is dropped.
	\item If the~entry is found and has the~\textit{drop when destination} flag set, the~frame associated with the~request is dropped.
	\item If the~frame associated with the~request has the~local-link destination \as{mac} address, either \textit{pass~all} parameter or \textit{pass~local~link} parameter must be set for the~source port to pass the~frame. If the~frame does not have the~local-link destination \as{mac} address, \textit{pass~all} parameter must be set for the~source port to pass the~frame, otherwise the~frame is dropped.
	% NOTE mlipinski
	% ML: wait, the~HASH table has a parameter "bpdu" which indicates whether the~MAC address in the~has table is link-local or not. If the~pass-all is unset for the~sourc port but pass-bpdu is set and the~HASH entry indicates it is bpdu, then the~frame is passed, otherwise it is dropped, right?
	% MZA: That's right.
	% if pass all set:
	%     frame not dropped
	% else:
	%     if pass bpdu set:
	%         if frame is bpdu:
	%             frame not dropped
	%         else:
	%             frame dropped
	%     else:
	%         frame dropped 
\end{enumerate}

\noindent \textbf{Topology resolution configuration verification}
\begin{enumerate}[resume]
	\item If the~topology resolution configuration verification is enabled and it indicates a drop flag for given \al{fid}, the~frame associated with the~request is dropped.
\end{enumerate}

\noindent \textbf{Final forwarding decision}
\begin{enumerate}[resume]
	\item  The~data set of the~forwarding engine response is specified in \ref{t:response}.
	\item If the~frame associated with the~request is intended to be dropped, the~forwarding engine response contains a port~ID parameter equal to the~source port~ID from the~request, a~\textit{drop}~flag set and the~rest of the~parameters is cleared.
	\item If the~frame associated with the~request is intended to be broadcasted, the~forwarding engine response contains a port~ID parameter equal to the~source port~ID from the~request, a~\textit{drop}~flag is unset and the~output port mask is restricted only by a~port mask configured for the~given \al{vid} and a~port mask resulting from a~topology resolution configuration.
	\item Otherwise, the~output port mask is additionally restricted by a~port mask read from the~MAC table entry found for destination MAC address.
	\item If the~request has an assigned priority, it becomes the~final priority of the~frame.
	\item Otherwise, if \as{vtab} entry for the~given \al{vid} has an assigned priority, it becomes the~final priority of the~frame.
	\item Otherwise, if \as{mtab} entry for source MAC address is found and has an assigned priority, it becomes the~final priority of the~frame.
	\item Otherwise, if \as{mtab} entry for destination MAC address is found and has an assigned priority, it becomes the~final priority of the~frame.
	\item Otherwise, the~default priority of the~frame is zero.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.92]{pic/forwarding-decision.pdf}
	\caption{The forwarding decision algorithm.}
	\label{fig:forwarding-decision}
\end{figure}

\section{Multiple Forwarding Engines}
\label{multiple-forwarding-engines}

\noindent According to project assumptions, the~switch is supposed to handle up to 360~\as{mpps}, which is the~result of supporting up to 24~ports with a~10~Gb/s link and working in a full-duplex mode. Consequently, the~forwarding engine must be ready to get new requests every 2.8~ns to ensure switch determinism. Additionally, the~forwarding decision must be provided within 67.2~ns, which is the~time it takes to receive a minimum-size frame (including interpacket gap).

%fully pipelined (requests received every clock cycle)
%24 ports per single engine (2.8ns) > 358MHz
%12 ports per single engine (5.6ns) > 179MHz / 2 forwarding engines
%
%requests received every two clock cycles
%12 ports per single engine (5.6ns) > 358MHz / 2 forwarding engines
% 6 ports per single engine (11.2ns) > 179MHz / 4 forwarding engines

Even with a fully pipelined dataflow architecture and accepting a new request in every clock cycle, the~forwarding engine must work with at least 358~MHz frequency.
That frequency is theoretically accessible for the~Xilinx Zynq Ultrascale+ \mbox{XCZU17EG-1FFVC1760E} device~\cite{ultrascale-datasheet}, but it is likely that timing constraints are not met when the~overall \al{wrs} firmware is implemented. Moreover, the~Xilinx \as{ip} Cores performance might restrict the~achievable frequency.

Under that circumstances, forwarding engines must be multiplicated to guarantee a~\mbox{wire-speed} throughput. The number of required forwarding engine instances is proportional to the~number of clock cycles when the~forwarding engine is not ready to accept subsequent request. In the~case of forwarding engine multiplication, array of the~identifiers of the~ports assigned to a~given engine instance is set as the~\as{hdl} module parameter.

Additionally, it should be noticed that a final forwarding decision also depends on the~\as{tru} module response. The~\as{tru} module can accept requests in every clock cycle and provides a~response within two clock cycles. However, it may be necessary to multiplicate the~\as{tru} module along with the~\as{tru} memory table.

\section{Forwarding Engine Architecture}

\noindent The flowchart depicted in figure \ref{fig:forwarding-decision} splits the~processing into subsequent stages:
\begin{itemize}
	\item initial configuration verification,
	\item \as{vlan} configuration verification,
	\item configuration for source \as{mac} address verification,
	\item configuration for destination \as{mac} address verification,
	\item topology resolution configuration verification,
	\item final forwarding decision.
\end{itemize}

The \as{vlan} configuration verification must precede further steps as the~\al{fid} read from the~\al{vtab} is, along with the~\as{mac} address, part of a~\al{htab} selector. Subsequently, both source and destination \as{mac} addresses are searched in a~\al{htab} and entries for both \as{mac} addresses are read from a~\al{mtab} within a single request processing. Moreover, only a single memory port is available for lookup - the~second one is used for updates from \af{ps}. Theoretically, searching could be performed sequentially using the~same memory table. 

Nevertheless, the~processing must be as much pipelined as possible in order to avoid unnecessary forwarding engines multiplication. Especially, the~necessity to read the~same memory block twice within processing a single request must be avoided, thereby tables are multiplied to achieve higher throughput -- it is more effective to multiply a~\al{htab} and a~\al{mtab} only than to multiply a whole forwarding engine module, including the~tables. The memory tables are implemented as the~Simple \mbox{Dual-Port} \as{ram}, because a~forwarding engine performs only read operations and \as{ps} performs only write operations. Then searching a~\al{htab} for the~entries associated with source and destination \as{mac} addresses, as well as topology resolution configuration verification, can be made in parallel. The final forwarding response is generated if all previous steps are done. 

The flowchart illustrates that the~decision to drop the~frame associated with the~request can be made at each stage of the~processing. Nonetheless, due to the~pipelined architecture and the~fact that responses must follow the~order of requests, even if a response is known in advance, further processing is not omitted.

The~project was developed using the~SystemVerilog\footnote{SystemVerilog, standarized as \as{ieee}~1800, is a hardware description and hardware verification language. It is based on Verilog language.} language. The~forwarding engine request and response data sets, along with the~\as{tru} module request and response and learning \as{fifo} entry data sets, are defined in the~appendix \ref{forwarding-engine-configuration}. The modular design and standardized \as{axi}4 interfaces are used to provide future-proof implementation and ensure that additional functionality can be easily added at later stages.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.67]{pic/wr-switch-new.pdf}
	\caption{The developed White Rabbit Switch forwarding engine architecture.}
	\label{fig:wr-switch-new}
\end{figure}

\subsection{VTAB Reader Module}

\noindent The~\as{vtab} reader module reads a~\al{vtab} entry corresponding to the~\al{vid} from the~request. Its parameter is a~\al{vtab} read latency given in clock cycles. On the~basis of the~forwarding engine request and the~read \al{vtab} entry the~module forwards:
\begin{itemize}
	\item part of the~request data set needed to make final forwarding decision (\texttt{M\_axis\_req}),
	\item part of the~\al{vtab} entry needed to make final forwarding decision (\texttt{M\_axis\_vtab}),
	\item \al{fid} and source \as{mac} address for HASH table lookup (\texttt{M\_axis\_htab\_src}),
	\item \al{fid} and destination \as{mac} address for HASH table lookup (\texttt{M\_axis\_htab\_dst}),
	\item learning \as{fifo} data set (\texttt{M\_axis\_learning\_fifo}),
	\item \as{tru} module request (\texttt{M\_axis\_tru}).
\end{itemize}

\subsection{HTAB Lookup Module}

\noindent The \as{htab} Lookup module is responsible for performing \al{htab} lookup for a given \al{fid} and \as{mac} address, according to the~\al{htab} structure presented in section \ref{fib-implementation}. Its parameter is a~\al{htab} read latency given in clock cycles. The module forwards a~found \al{htab} entry or sets a~not found flag on an~output signal \texttt{tuser}.

\subsection{MTAB Reader Module}

\noindent The~\as{mtab} reader module reads a~\al{mtab} entry from the~address contained in a~found \al{htab} entry. Its parameter is a~\al{mtab} read latency given in clock cycles and a~source/destination flag indicating which fields from a~read \al{mtab} entry should be taken into account\footnote{For instance, \texttt{drop\_when\_source} or \texttt{drop\_when\_destination}.}. The module forwards a data set needed to make a~final forwarding decision, including a~drop flag when a~\al{htab} entry has not been found or when the~\as{fid} and the~\as{mac} address from a~\al{htab} entry are non consistent with the~\as{fid} and the~\as{mac} address from a~\al{mtab} entry.
Additionally, the~\as{mtab} reader instance dedicated for reading a~source MAC entry updates the~ageing bitmap if a~\al{htab} entry has been found.

\subsection{Ageing Bitmap}

\noindent The~ageing bitmap is stored in \as{bram} and has its shadow memory. Currently active bitmap is pointed by forwarding engine configuration parameter (\texttt{ageing\_bitmap\_ptr}). Each slot in a~\al{htab} is represented by a~single bit, which is set when the~entry from the~slot is found. An~ageing bitmap is read by a~Zynq Processing System at intervals in the aim to remove stale entries from \as{htab} and \as{mtab} tables. Before reading a~bitmap, the \texttt{ageing\_bitmap\_ptr} parameter is switched, so the~bitmap being read is not updated.
After reading a~bitmap it is cleared.

\subsection{Forwarding Decision Maker Module}

\noindent The Forwarding Decision Maker module is responsible for making a~final forwarding engine decision on the~basis of:
\begin{itemize}
	\item forwarding engine configuration,
	\item forwarding engine request data set (\texttt{S\_axis\_req}),
	\item \al{vtab} entry (\texttt{S\_axis\_vtab}),
	\item \al{mtab} entry for source \as{mac} address or not found flag (\texttt{S\_axis\_mtab\_src}),
	\item \al{mtab} entry for destination \as{mac} address or not found flag (\texttt{S\_axis\_mtab\_dst}),
	\item \as{tru} module response, if \as{tru} processing enabled (\texttt{S\_axis\_tru}).
\end{itemize}
It is assumed that all enumerated data sets are received in the~same clock cycle.
The module implements the~algorithm described in section \ref{forwarding-decision-algorithm} and determines forwarding engine response components: drop flag, output port mask and frame priority. Besides, if a~\al{htab} entry for a~source \as{mac} address has not been found and a~learning process is enabled, it forwards a~learning \as{fifo} data set (received on \texttt{S\_axis\_learning\_fifo} interface) to a~learning \as{fifo}.

\section{Communication with the~Zynq Processing System}

\noindent A~forwarding engine module implemented in a \af{pl} is supposed to be connected to a~Zynq \al{ps} through the~\as{axi} ports. The communication comprises:
\begin{itemize}
	\item writing to \as{vtab}, \as{htab} and \as{mtab} tables,
	\item writing to registers bank (containing a forwarding engine module configuration signals),
	\item reading aging bitmap,
	\item receiving entries from learning \as{fifo},
	\item receiving interrupt signals.
\end{itemize}

The efficient tables updates are supposed to be performed by the~\al{ps} due to keeping the~contents of tables as a~shadow memory in \as{ps} \as{ram}. To provide atomic updates even with separate \as{htab} and \as{mtab} tables, the~deletion of a~selector from a~\as{htab} must precede the~deletion of an~entry from a~\as{mtab}. On the~contrary, in the~case of insertion operation writing an~entry to a~\as{mtab} must be performed first.

% NOTE
% timeline:
% 1) read HTAB entry (PL)
% 2) clear HTAB entry (PS)
% 3) clear MTAB entry (PS)
% 4) read MTAB entry (PL) 

% TODO
%The block design depicted in figure \ref{fig:pl-ps} presents the~example AXI infrastructure that connects PS with a forwarding module. 
%
%\begin{figure}[H]
%	\centering
%	\includegraphics[scale=0.67]{pic/wr-switch-new.pdf}
%	\caption{The developed White Rabbit Switch forwarding engine architecture.}
%	\label{fig:pl-ps}
%\end{figure}

\section{Forwarding Engine Architecture Alternatives}
\label{forwarding-engine-architecture-alternatives}

\noindent The \al{wrs} firmware has not been adapted to the~increased number of ports and their speed yet. The developed forwarding engine is the~first module which addresses the~change in the~required performance. Therefore, its submodules are as modular as possible. Such approach allows for relatively convenient project modifications when the~frequency or available resources differ from the~assumptions.
The~potential changes are mentioned below.
\begin{itemize}
	\item The developed forwarding engine is fully pipelined and ready to receive requests in every clock cycle. Thus, the~\as{axi}4 Stream interfaces do not include \texttt{tready} signal -- it is assumed to be always high. If it is acceptable for forwarding engine not to process requests at the~wire-speed, the~support for \texttt{tready} signal is required. 
	\item If \as{fid} and \as{mac} addresses are limited to a~set of values known in advance, \as{crc} codes used for hash sub-tables indexes calculation may be potentially optimized.
	\item If it can be assumed that the~latency between reading a~\as{htab} and a~\as{mtab} for a~single request is lower than the~latency between updating a~\as{htab} and a~\as{mtab}, storing a~selector in a~\as{mtab} is redundant.
	\item In the~case of tables' size changes, the~parameters of the~\al{bram} in UltraScale devices, described in~\cite{ultrascale-memory}, should be taken into account to provide optimal memory resources usage.
	\item In the~case of tables' size changes, it may be more beneficial to store \al{mtab} entries in a hash table, as analysed in \ref{s:hash-collisions-simulations}.
	\item \al{bram} resources may be replaced with \al{uram} resources and vice versa.
	\item In a~proposed architecture, the~number of \asp{htab} and \asp{mtab} is equal to $2f$, where $f$ is the~number of forwarding engine instances. The memory resources may be reduced to ${f+1}$ \asp{htab} and \asp{mtab} by implementing them as True \mbox{Dual-Port} RAM memory blocks, kept in a shared bank of tables, only one of which could be updated at a time. However, as a~consequence, there would be no guarantee that all memory table instances have always the~same contents.
\end{itemize}



\section{Filtering Database Structure}
\label{s:filtering-database-structure}

\noindent In the~previous chapter the~Filtering Database implementation criteria were summarized and the~number of~\mbox{\as{fpga}-based} high performance implementations have been proposed. Their review along with the~most important design assumptions, enumerated below, was the~basis for the~selection of~the~target Filtering Database structure.
\begin{itemize}
	\item The~Filtering Database is supposed to be stored in embedded, high bandwidth memory (\al{bram} and/or \al{uram}) and around 19 Mb\footnote{${1/3 \cdot 28}$~Mb of~total \al{bram} resources and ${1/3 \cdot 28.7}$~Mb of~\af{uram} resources.} is available. High memory utilization is one of~the~most important Filtering Database structure aspects.
	% NOTE mlipinski
	% ML: minus the memory required for the VLAN table
	% MZA: As defined in IEEE 8021Q, "Filtering Database" is the term for both MAC and VLAN entries.
	\item Network configuration is not supposed to be modified frequently. Thus, the~lookups to the~insertions ratio is high and most lookups are supposed to be successful.
	\item The~forwarding engine must be able to perform lookup for every single request, even if the~maximum number of~frames per second is received on each of~the~switch ports. Therefore, deterministic worst-case lookup time is critical.
	\item The~\al{wrs} is a~\mbox{\as{vlan}-aware} device and supports shared \as{vlan} learning. In~terms~of implementation it implicates separate memory tables for mapping \as{mac} addresses and \as{fid}s to device ports and for \as{vlan} configuration.
	\item Current Filtering Database entries are defined in \cite{wrs-hdl-sw}. For backward compatibility, changes will be introduced only if necessary. 
	\item Insertion algorithm should not require using shadow memory - probably there is no enough memory resources for that.
\end{itemize} 

\noindent Taking the~above considerations into account, primary conclusions may be drawn:
\begin{itemize}
	\item A~single MAC address configuration entry has 16~B (as specified in appendix~\ref{filtering-database-entries}). Thus, a~\af{mtab} size must be at least 160~KB (up to 10K entries), which constitutes 33 \as{bram}s or 6 \as{uram}s for UltraScale+ devices, out of 796 BRAMs and 102 URAMs available on Xilinx Zynq Ultrascale+ XCZU17EG-1FFVC1760E.
	\item A~single \as{vlan} configuration entry has 8~B (as specified in appendix~\ref{filtering-database-entries}). The~12-bit \al{vid} has 4096 possible values, which correspond to \al{vtab} entries' indexes. Therefore, the~configuration for the~specific \al{vid} may be read directly, and no search mechanism is needed. Thus, a~\al{vtab} size must be 32~KB (4K entries), which constitutes 4.5 \as{bram}s or 1 \as{uram} for UltraScale+ devices.
	\item The~hash table structure is bound to store \al{mtab} and d-left hashing schema is chosen to reduce the~probability of~unresolved hash collisions. Such a~solution fulfills conditions concerning high memory utilization and deterministic worst-case lookup time. Additionally, it allows for taking advantage of~possibility of~parallel access to embedded memory blocks. 
	\item Each sub-table must be a~separate memory block to read them in parallel. 
\end{itemize}
\ \\
\noindent It is beneficial in terms of~memory utilization to separate a~hash table containing selectors only from a~\al{mtab} containing selector assigned configuration. Then a~\al{mtab} contains only 10K entries and a~\al{htab} contains $S > 10K$ entries to obtain appropriately low collision probability. Such a~separation may be introduced at the~cost of~additional forwarding process latency. With \as{fid} and \as{mac} address as a~selector, an~\al{mtab} entry specified in the~appendix~\ref{filtering-database-entries} and up to 10K active nodes in a~network, the~separation is beneficial if $S > 12.4K$. However, the~advantage is smaller if a~\al{mtab} is supposed to store selector as well to guarantee an atomic read operation. Then, the~separation is beneficial if $S > 23.9K$.
% NOTE
% selector in MTAB:
% S * 72b + 10K * 124b < S * 124b
% S  > 23.9K 
% no selector in MTAB:
% S * 72b + 10K * 64b < S * 124b
% S > 12.4K

The alternative Filtering Database structures are depicted in figures \ref{fig:filtering-database-structure-separate} and \ref{fig:filtering-database-structure}. The~aspects that cannot be predicted and should be emulated with the~use of~the~reference software design comprise: the~number of~sub-tables -- $d$, hash functions used -- $h_{1}, h_{2}, ..., h_{d}$, the~number of~buckets in a~single sub-table -- $m$, the~number of~slots per bucket -- $s$ and, thus, the~overall number of~slots in a~hash table -- $S = d \cdot m \cdot s$.

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{pic/filtering-database-structure-separate.pdf}
	\caption{The Filtering Database Structure - separate HTAB and MTAB.}
	\label{fig:filtering-database-structure-separate}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{pic/filtering-database-structure.pdf}
	\caption{The Filtering Database Structure - MTAB as a~hash table.}
	\label{fig:filtering-database-structure}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE
% \cite{ultrascale-memory}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BRAM
% Single block up to 36 Kbits, two write and two read ports, one 36Kb / two 18Kb.

% 36Kb:        18Kb:

% 32K x 1b     
% 16K x 2b     16K x 1b
%  8K x 4b      8K x 2b
%  4K x 9b      4K x 4b
%  2K x 18b     2K x 9b
%  1K x 36b     1K x 18b (only if True Dual Port RAM)
% 512 x 72b    512 x 36b (only if Simple Dual Port RAM)

% HASH table : 1/2/4K x 72b (FID + MAC + pointer to one of~10K addresses of~MAC table)
% MAC table: 10K x 124b (min 118b)
% VLAN table:  4K x 40b (min 38b)

% 1/3 * 600 BRAM = 200 BRAM = 14 Mb?
% BRAM / UltraRAM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% URAM
% Single block 288Kbits, dual port, 4K x 72b

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Hash Functions}

\noindent On the~whole, the~choice of~a~hash function for a~hash table should be based on two most important criteria -- uniformity and low computational cost~\cite{aoc}. 

Uniform hash function provides mapping of the~expected keys as evenly as possible over its output range. In other words, for the~$n$-bit hash function the~probability for each possible result should be roughly the~same -- $1/2^n$. Using hash functions with relatively uniform distribution is the~measure to reduce the~probability of~hash collisions.

The uniformity of~the~distribution of~hash function output values may be evaluated with the~chi-squared test. In general, the~chi-squared test is used to determine whether an observed sample data is consistent with a~hypothesized distribution. In the~case of~verifying hash functions for hash table usage, a~null hypothesis $H_{o}$ states that the~data are not consistent with a~uniform distribution. Thus, the~expected number of~items for bucket $ith$ equals to $n/k$, where $n$ is the~total sample size and $k$ is the~number of~possible hash function output values. The~test statistic ($\chi^{2}$) is a~chi-square random variable defined by the~equation:
\begin{equation}
\chi^{2} = \sum_{i=1}^{k}{\frac{(O_{i} - E_{i})^2}{E_{i}}}
\end{equation}
where $O_{i}$ is the~observed number of~items assigned to bucket $ith$ and $E_{i}$ is the~expected number of~items assigned to bucket $ith$. A low value for chi-square statistic means there is a~high correlation between sample data and expected results. The~obtained test statistic is compared to the~chi-squared distribution with $k$ degrees of~freedom to get a~P-value. The~P-value is the~probability of~observing a~sample statistic as extreme as the~test statistic. The~null hypothesis $H_{o}$ is rejected when a~P-value is less than the~significance level, which theoretically may have any value between 0 and 1, but usually is equal to 0.01, 0.05, or 0.10. 

Since the~document \as{rfc} 2889 (\textit{Benchmarking Methodology for LAN Switching Devices}) recommends using sequential addresses for lookup table capacity tests \cite{rfc2889}, it is observed that hash functions used in commercially available switches are optimized for storing sequential keys rather than random ones \cite{effect}. Although this approach is beneficial in a~marketing context, the~use of~solely sequential addresses in real-life networks is quite unlikely. Therefore the~experiments with the~use of~generated random addresses or data~from statistical analysis of~Ethernet traffic are more reliable and valuable.

Hash function computational cost involves required logic resources for hardware implementation and number of~clock cycles needed to perform a~computation. For instance, for purposes of~hash tables all cryptographic functions performance is unacceptable. The~optimal solution for network applications in terms of~computational cost along with the~uniformity, and amenable to hardware implementation, are \af{crc} codes~\cite{crc, hash-function}.

\section{Hash Collisions Simulations}
\label{s:hash-collisions-simulations}

\noindent Primarily, the~uniformity of~chosen \al{crc} codes is examined with the~use of~the~chi-squared test, as described in the~previous section. Assuming that number of~buckets in each of~hash sub-tables is $m \geq 1024$, only \as{crc} codes with 10 or more bits are taken into account. Additionally, for \as{crc} codes with more than 10 bits only the~lowest 10 bits of~the~hash function output are regarded as a~result. The~examined \as{crc} codes with their polynomial representations are listed below\footnote{The \as{crc} codes implementation used for test is a~python library \texttt{crccheck} (\url{https://pypi.org/project/crccheck}, accessed \today). The~detailed specification of~the~\as{crc} codes is contained on \url{https://reveng.sourceforge.io/crc-catalogue}, accessed \today.}:
\begin{itemize}
	\item CRC-10 (\texttt{0x233}),
	\item CRC-11 (\texttt{0x385}),
	\item CRC-16-CCITT (\texttt{0x1021}),
	\item CRC-16-CDMA2000 (\texttt{0xC867}),
	\item CRC-16-DECT (\texttt{0x0589}),
	\item CRC-16-IBM (\texttt{0x8005}),
	\item CRC-32 (\texttt{0x04C11DB7}),
	\item CRC-32C (\texttt{0x1EDC6F41}),
	\item CRC-32-AIXM (\texttt{0x814141AB}).
\end{itemize}

For each hash function and for a~generated set of~100K unique selectors\footnote{concatenation of~\as{fid} and \as{mac} address}, hashing of selectors into 1024 buckets was performed. The~test outcome, presented in table \ref{t:uniformity}, comprises:
\begin{itemize}
	\item minimum count of~keys assigned to a~bucket,
	\item maximum count of~keys assigned to a~bucket,
	\item the~standard deviation of~keys in buckets distribution,
	\item the~chi-squared statistic of~keys in buckets distribution,
	\item the~P-value for calculated chi-squared statistic.
\end{itemize}

\begin{table}[H]
	\centering
	\small
	\caption{Hash function uniformity example test results.}
	\label{t:uniformity}
	\begin{tabular}{||m{0.3\textwidth}|
			>{\centering\arraybackslash}m{0.1\textwidth}|
			>{\centering\arraybackslash}m{0.1\textwidth}|
			>{\centering\arraybackslash}m{0.1\textwidth}|
			>{\centering\arraybackslash}m{0.1\textwidth}|
			>{\centering\arraybackslash}m{0.1\textwidth}||} 
		\hline
		\textbf{hash function} & \textbf{min} & \textbf{max} & \textbf{std} & \textbf{$\chi^{2}$} & \textbf{P-value} \\ \hline\hline
		
		\texttt{CRC-10}          & 69 & 201 & 10.42 & 1135.45 & 0.01 \\ \hline
		\texttt{CRC-11}          & 64 & 195 & 10.42 & 1136.87 & 0.01 \\ \hline
		\texttt{CRC-16-CCITT}    & 69 & 192 & 10.06 & 1058.63 & 0.21 \\ \hline
		\texttt{CRC-16-CDMA2000} & 70 & 185 &  9.97 & 1040.60 & 0.34 \\ \hline
		\texttt{CRC-16-DECT}     & 62 & 190 & 10.29 & 1107.26 & 0.03 \\ \hline
		\texttt{CRC-16-IBM}      & 66 & 180 & 10.00 & 1046.27 & 0.29 \\ \hline
		\texttt{CRC-32}          & 64 & 184 &  9.88 & 1022.21 & 0.49 \\ \hline
		\texttt{CRC-32C}         & 71 & 185 & 10.47 & 1147.12 & 0.03 \\ \hline
		\texttt{CRC-32-AIXM}     & 68 & 201 & 10.30 & 1109.59 & 0.00 \\ \hline
	\end{tabular}
\end{table}

The~significance level for the~P-value is defined as 0.05. Thereby, based on example test results, the~hash functions that provide most uniform distribution are \texttt{CRC-16-CCITT}, \texttt{CRC-16-CDMA2000}, \texttt{CRC-16-IBM}, \texttt{CRC-32}. 

Nevertheless, it should be emphasized that subsequent test iterations do not produce persistent results and the~chi-squared statistic for given hash functions and the~same number of~random, unique selectors significantly varies. It is observed that the~\as{crc} code choice is more purposeful when the~selectors' values are not random but follow a~specific pattern. Thus, for only a~few \as{fid}s configured for device and network \as{mac} addresses within a~specific range it is advised to re-run the~test and to potentially adjust the~\as{crc} codes. Since no restrictions for \as{fid} and \as{mac} address values are given as project assumptions, it is presumed that all examined \as{crc} codes are equally suitable for hash sub-tables indexes calculation.

Besides, as outlined in section \ref{s:filtering-database-structure}, the~number of~sub-tables ($d$), the~number of~buckets in a~single sub-table ($m$), and the~number of~slots per bucket ($s$) are supposed to be selected.
For different sets of~these parameters, randomly generated 10K unique selectors are assigned to appropriate slots. The~selectors that cannot be put into any sub-table due to unresolved collision are counted. The~results of~100 iterations of~simulation test, summarized in table \ref{t:structure}, are consistent with the~theory of~hash collisions resolution methods described in chapter~\ref{state-of-art} -- it is more beneficial to append an~additional sub-table (which uses different hash function for index calculation) than to append slots to buckets in existing hash sub-table. 

Besides, a single \as{uram} block in UltraScale architecture-based devices is a~dual port memory, 4096 depth and 72 bits width~\cite{ultrascale-memory}. The number of buckets ${m = 4096}$ per hash subtable allows for the~efficient use of \as{uram} memory resources.
Thus, taking into account the~characteristics of~the~Xilinx Zynq Ultrascale+ XCZU17EG-1FFVC1760E device and the~simulation test results, and assuming that ultimately selectors will not be stored in a~\al{mtab}, following parameters are chosen: ${d = 5}$, ${m = 4096}$, ${s = 1}$.
The~hash sub-tables indexes are calculated as 11 lowest bits of~outputs of~hash functions: \mbox{${h_{1} = }$ \texttt{CRC-16-CCITT}}, \mbox{${h_{2} =}$ \texttt{CRC-16-CDMA2000}}, \mbox{${h_{3} =}$ \texttt{CRC-16-IBM}}, \mbox{${h_{4} =}$ \texttt{CRC-32}} and \mbox{${h_{5} =}$ \texttt{CRC-32C}}. The~trade-off between obtained average number of~unresolved collisions for storing 10K entries in a~\al{mtab} and needed memory resources is regarded as acceptable.

\begin{table}[H]
	\centering
	\small
	\caption{Hash table structure test results.}
	\label{t:structure}
	\begin{tabular}{||
			>{\centering\arraybackslash}m{0.13\textwidth}|
			>{\centering\arraybackslash}m{0.09\textwidth}|
			>{\centering\arraybackslash}m{0.06\textwidth}|
			>{\centering\arraybackslash}m{0.16\textwidth}|
			>{\centering\arraybackslash}m{0.16\textwidth}|
			>{\centering\arraybackslash}m{0.09\textwidth}|
			>{\centering\arraybackslash}m{0.09\textwidth}||} 
		\hline
		\textbf{sub-tables (d)} & \textbf{buckets (m)} & \textbf{slots (s)} & \textbf{average number of~unresolved collisions} & \textbf{probability of~unresolved collision} & \textbf{BRAM usage} & \textbf{URAM usage} \\ \hline\hline
		
		10 & 1024 & 1  &  388.33 & 100.0 \% & 20 & 10 \\ \hline
		 1 & 1024 & 10 & 1151.96 & 100.0 \% & 20 & 10 \\ \hline
		10 & 2048 & 1  &    0.00 &   0.0 \% & 40 & 10 \\ \hline
		 8 & 2048 & 1  &    0.02 &   2.0 \% & 32 &  8 \\ \hline
		 5 & 2048 & 2  &    0.00 &   0.0 \% & 40 & 10 \\ \hline
		 4 & 2048 & 2  &    1.21 &  67.0 \% & 32 &  8 \\ \hline
		 2 & 2048 & 4  &   32.23 & 100.0 \% & 32 &  8 \\ \hline
		 1 & 2048 & 8  &  218.24 & 100.0 \% & 32 &  8 \\ \hline
		 4 & 4096 & 1  &   91.59 & 100.0 \% & 32 &  4 \\ \hline
		 5 & 4096 & 1  &    0.91 &  61.0 \% & 40 &  5 \\ \hline
	\end{tabular}
\end{table}

Additionally, the figures \ref{fig:avg} and \ref{fig:p} compare simulation test results -- the average number of~unresolved collisions and the probability of unresolved collision -- for 4 and 5 hash sub-tables and subsequent number of selectors stored in a MAC table. In the~case of the~lack of memory resources for the~overall \al{wrs} gateware, decreasing number of hash sub-tables may be considered.

\begin{figure}[H]
	\centering
	\includesvg[scale=0.25]{pic/hash-table-structure-avg}
	\caption{The average number of unresolved collisions.}
	\label{fig:avg}
\end{figure}

\begin{figure}[H]
	\centering
	\includesvg[scale=0.25]{pic/hash-table-structure-prob}
	\caption{The probability of unresolved collision.}
	\label{fig:p}
\end{figure}

%number of selectors: 4000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 0.0, probability of collisions: 0.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.0, probability of collisions: 0.0%
%number of selectors: 5000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 0.12, probability of collisions: 11.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.0, probability of collisions: 0.0%
%number of selectors: 6000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 0.56, probability of collisions: 43.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.0, probability of collisions: 0.0%
%number of selectors: 7000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 3.55, probability of collisions: 96.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.0, probability of collisions: 0.0%
%number of selectors: 8000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 12.9, probability of collisions: 100.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.01, probability of collisions: 1.0%
%number of selectors: 9000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 37.46, probability of collisions: 100.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 0.18, probability of collisions: 16.0%
%number of selectors: 10000
%sub-tables: 4, buckets: 4096, slots per bucket: 1, avg collisions: 93.21, probability of collisions: 100.0%
%sub-tables: 5, buckets: 4096, slots per bucket: 1, avg collisions: 1.2, probability of collisions: 74.0%
%TODO {reference to the~WRS (including its software and firmware) version which is a~base for my project}

\noindent  The~\al{wr} network is a~Bridged Local Area Network with \as{vlan}s, which is defined in the~\as{ieee}~802.1Q standard. Its topology is based on spanning tree architecture with the~master switch at the~root, which is the~time source for all devices in the~network~\cite{lipinski}. The~master switch may be synchronized to the~\af{utc} by a~\af{gps} receiver and/or \af{ntp}~\cite{space}.

The~architecture of the~main networking components of the~\acrshort{wr} system -- switches and nodes -- is compliant with the~\as{ieee}~802 standards and can be characterised using the~\as{osi} model. \as{wr} switch implements physical and data link layers of the~\as{osi}~model thus being a~\af{l2} switch, as explained in section \ref{ss:osi}. The~switches interconnect network endpoints, such as \as{wr} nodes, as outlined in figure \ref{fig:wr-network}. There is no specific hierarchy -- any endpoint is able to communicate with any other.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=1.2]{pic/wr-network.png}
	\caption{ The~White Rabbit network schema. \textit{Source:} \cite{wr-wiki}.}
	\label{fig:wr-network}
\end{figure}

The~detailed architecture and functionalities of \as{wr} nodes are application-specific, but all systems use the~very same design of the~\as{wr} switch~\cite{lipinski}, which can be called the~heart of each \as{wr}-based network. Its architecture and implementation are the~decisive factors to meet the~requirements specified in table \ref{t:wr-system-requirements}.

The~time-sensitive functionalities of the~White Rabbit switch that influence its latency are implemented in \as{fpga} gateware, while not time-critical ones are implemented in software. Current \as{wr} switch hardware release (version~3.4) 
%TODO link do repo?
supports both fiber and copper connections and up to 18 SFP Gigabit ports. It is compatible with official firmware and software (version~5.0) 
%TODO link do repo?
and is licensed under \af{ohl}~1.2.

Similarly to standard Ethernet \as{l2} switches, the~\al{wrs} is a~network bridge that uses \as{mac} addresses to forward Ethernet frames between its ports. However, unlike a~regular switch, a~\as{wr} switch additionally has a~sub-nanosecond synchronization engine, which allows hierarchical distribution of time in \as{wr} networks. Due to the~fact that the~scope of the~following thesis is limited to the~implementation of a~forwarding engine, the~description below focuses on the~\as{wr} switch components that have a~significant impact on this process. The~other blocks are briefly mentioned to explain their function in the~\as{wr} switch architecture. Drawing \ref{fig:wrs} shows the~simplified functional diagram of a~two-port \as{wr} switch and a~\as{wr} node. 

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.272]{pic/wrs.png}
	\caption{Simplified architecture of a two-port White Rabbit Switch. \textit{Source:}~\cite[figure 1.5]{lipinski}.}
	\label{fig:wrs}
\end{figure}

\section{White Rabbit Switch Forwarding Process}
\label{wrs-forwarding-engine}

\noindent An Ethernet frame received on one of the~switch's ports is first processed by the~\as{hdl} module named Endpoint, which implements the~functionalities of a~port defined in \as{ieee}~802.1Q and \as{ieee}~802.3 standards. One of the~module's key functions is parsing the~Ethernet frames and decoding information from their headers, including \as{mac} addresses, \al{vid} and priority. These frames are then forwarded to two cooperating modules implemented in gateware -- extracted header information is transmitted to the~\af{rtu} while the~frame is buffered in the~Swcore Multi-Access Memory.

\al{rtu} is a~forwarding engine that decides to which ports a~frame should be sent. In general the~decision is based on the~\af{vtab} record that matches \al{vid} and on the~\af{htab} record that matches \as{mac} addresses and the \al{fid} read previously from the~\al{vtab}. The~tables' entries are managed by the~\as{rtu} deamon running on the~\as{cpu}. The~\al{vtab} content is based on the~device \as{vlan} configuration set by the~user. The~\al{htab} stores dynamic and static entries mapping \as{mac} addresses and \as{vlan}s to the~ports through which the~frame's recipient is reachable. 

Nevertheless, the~\al{rtu} is not able to provide forwarding decision within the~time of receiving a~minimum-size Ethernet frame when such frames are being received on all the~switch ports at full bandwidth. The~bottleneck of the~whole system is the~operation of looking up \as{mac} addresses in the~\al{htab} in the~\as{rtu} submodule named \textit{Full Lookup Engine}. The~requests to the~\textit{Full Lookup Engine} are not pipelined. As a~consequence, latency of generating response depends on the~traffic load and thus is not deterministic.

However, in \as{wr}-based systems only some traffic classes are considered critical: broadcast or multicast frames from the~master node to all nodes within a~\as{vlan} and multicast frames from a~node to a~master node. All the~critical traffic is sent within a~pseudo-multipath spanning tree defined by a~\al{vid}. For that reason, it may be processed separately by another \as{rtu} submodule, \textit{Fast Lookup Engine}, which bases its decision on the~\al{vtab} entries and skips looking up the~\al{htab}. Such solution guarantees a~determinism in regard to a~latency of a~selected traffic. There is a~possibility to configure additional types of traffic as fast-forwarding, thereby using only fast lookup mechanism that is based on \as{vid}s:
\begin{itemize}
	\item single \as{mac} addresses (unicast/multicast),
	\item ranges of \as{mac} addreses (unicast/multicast),
	\item broadcast,
	\item link-local protocols.
\end{itemize}

To ensure determinism of the~switch, the \as{rtu} \textit{Port} submodules control processing of the~non-fast-forwarding traffic in parallel by both lookup engines. In case of the~next request arrival before generating the~response by the~Full Lookup Engine, only the~decision from the~Fast Lookup Engine is taken into account. Otherwise, a~final forwarding decision is a~combination of both engines' outputs.

% NOTE
% Port submodule
% - represents each switch's port (endpoint)
% - takes requests from a given port
% - forwards the request to request FIFO (round robin)
% - awaits the answer from RTU engine
% - outputs response to the port which requested it (endpoint)

The~Fast Lookup Engine is supported by another gateware module, the~\af{tru}. The~\as{tru} provides a~hardware support for topology resolution protocols implemented in software. The~requests to the~\as{tru} are pipelined and each one is processed within two clock cycles. The~response contains the~forwarding mask, which restricts the~\as{rtu} decision in the~aim of preventing loops and properly managing redundant ports. Besides, the~\as{tru} module speeds up the~reconfiguration of \as{vid}-based forwarding rules after failure detection to a~single clock cycle. It is achieved by the~direct access to the~switch ports' state information and by keeping active as well as backup configuration rules in a~memory implemented in gateware. The~\as{tru} communicates with the~Endpoints also to turn the~port off when its failure is detected and to verify if the~port is stable when it goes up.

When the~\as{rtu} response is ready it is passed to the~Swcore module, which starts forwarding frame to appropriate Endpoints. Before the~Ethernet frames are forwarded, they are temporarily buffered in a~specialised, multi-port and multi-access memory that is a~part of the~Swcore module. It allows all the~Endpoints to read and write frames concurrently. The~frames are splitted in small chunks and written to the~memory fragments called pages, which are allocated dynamically by the~Page Allocator gateware module.

In the~aim of making a~critical traffic latency independent from best-effort traffic, part of the~Swcore Multi-Access Memory resources is dedicated for processing a~critical traffic only. Before a~frame is not classified as best-effort or critical, it is stored in pages categorized as unknown. The~number of unknown pages ensure that any potentially critical frame will not be discarded. Frames already classified as critical are stored in dedicated resources and, later on, forwarded 
to output queues. The~\textit{Output Queue Scheduler} module manages the~order of frames transmissions depending on their priority. To make this possible, each of the~priorities has its own dedicated buffers. To guarantee low latency and determinism, the output queues implement option in which the~current transmission of a~non-critical frame is interrupted when a~critical frame is waiting to be sent.

The diagram of the \al{wrs} modules that take part in the~forwarding process is depicted in figure \ref{fig:wr-switch}. The main focus is put on the gateware architecture of the~\al{rtu} module.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.67]{pic/wr-switch.pdf}
	\caption{The diagram of the \as{wrs} modules that take part in the~forwarding process.}
	\label{fig:wr-switch}
\end{figure}

\section{White Rabbit Switch Synchronization Mechanism}
\label{wrs-synchronization}

\noindent  The~\al{wr} synchronization mechanism is based on the~\as{ieee}~1588 technology. It enhances the~standard with the~concepts of precise knowledge of the~link delay model and clock physical \af{l1} syntonization, similar to SyncE. \as{wr}-\as{ptp} extension defines \as{wr} \as{ptp} profiles that uses \as{ptp} Signaling messages and user-definable \textit{Type-Length-Value} fields to exchange \as{wr}-specific information over the~link~\cite{lipinski}. 

Instead of being forwarded between switch ports, received \as{ptp} messages are precisely timestamped in the~Endpoint with the~help of the~\af{ddmtd} module and sent to the~\as{cpu} that implements the~\as{wr} \as{ptp} protocol. \as{ptp} daemon uses these timestamps to calculate updates for the~software implementation of \as{wr} phase-locked loop, called SoftPLL, which controls the~frequency and phase of the~\as{wr} switch~\cite{lipinski}.

%Syntonization: local clock tuning based on a~measure of the~error between two clocks. 
%In White Rabbit, the~external clock and the~internal reference are compared.
%L1 syntonization is used as a~customized version of Sync-E to transmit the~clock over the~optical links. It uses local VCXO to syntonize the~local clock to the~recovered clock from the~link (Slave role).